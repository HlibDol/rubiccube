﻿using System;
using UnityEngine;

namespace Assets.Scripts.Src.Abstraction
{
    interface IRotationController
    {
        void RotateSides(string sideName, Vector3 startCoord, Vector3 curCoord, TouchPhase touchPhase);
    }
}

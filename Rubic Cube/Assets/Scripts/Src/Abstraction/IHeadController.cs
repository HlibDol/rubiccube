﻿using System.Collections.Generic;
using UnityEngine;

namespace RubicCubeApi.Abstractions
{
    interface IHeadController
    {
        void ChangePositionFor(string sideName, int startCoord, int curCoord, TouchPhase touchPhase);
        void SaveToDb(List<int> cubes);
    }
}

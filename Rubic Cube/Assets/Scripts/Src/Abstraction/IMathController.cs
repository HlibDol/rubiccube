﻿using UnityEngine;

namespace Assets.Scripts.Src.Abstraction
{
    interface IMathController
    {
        void ChangePosition(string sideName, int startCoord, int curCoord, TouchPhase touchPhase);
    }
}

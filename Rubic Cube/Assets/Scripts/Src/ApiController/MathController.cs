﻿using Assets.Scripts.Src.Abstraction;
using Assets.Scripts.Src.Calculations;
using Assets.Scripts.Src.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Src.ApiController
{
    internal class MathController : IMathController
    {
        private List<Cube> _cubes = null;

        private MathApi _math = null;

        public void ChangePosition(string sideName, int startCoord, int curCoord, TouchPhase touchPhase)
        {
            _math.ReCalculatePosition(sideName, startCoord, curCoord);

            if (touchPhase == TouchPhase.Ended || touchPhase == TouchPhase.Canceled)
                _math.SmoothScroll(startCoord, curCoord);
        }
    }
}

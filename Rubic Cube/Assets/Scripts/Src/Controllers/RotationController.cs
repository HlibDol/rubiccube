﻿using Assets.Scripts.Src.Abstraction;
using Assets.Scripts.Src.Models;
using RubicCubeApi.Abstractions;
using RubicCubeApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Src.Controllers
{
    internal class RotationController : IRotationController
    {
        private IHeadController _apiController = null;

        internal RotationController()
        {
            _apiController = new ApiHeadController();
        }

        public void RotateSides(string sideName, Vector3 startCoord, Vector3 curCoord, TouchPhase touchPhase)
        {
            throw new NotImplementedException();
        }

        private void RotateCubesUI(List<Cube> cubesToRotate)
        {

        }
    }
}

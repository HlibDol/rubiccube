﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Src.Controllers
{
    internal class GameController : MonoBehaviour
    {
        private SideController _panel = null;
        private GameObject _rubicCube = null;
        private void Start()
        {
            _panel = GetComponent<SideController>();
            _rubicCube = GameObject.Find("RubicCube");
            Input.simulateMouseWithTouches = true;
        }

        private void Update()
        {
            RotateRubicCube();

            if (Input.touchCount > 0)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.name.Contains("Panel"))
                {
                    //hit.point
                    //hit.transform.gameObject.transform.
                    //_panel.Go();
                }
                else
                {
                    RotateRubicCube();
                }
            }
        }

        private void RotateRubicCube()
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {                
                {
                    var x = Input.GetAxis("Mouse X") * Mathf.Deg2Rad * 200;// Input.mousePosition.y * Mathf.Deg2Rad;// touch.position.x;
                    var y = -Input.GetAxis("Mouse Y") * Mathf.Deg2Rad * 200;// Input.mousePosition.x * Mathf.Deg2Rad;// touch.position.y;                                   
                    var z = -Input.GetAxis("Mouse Z") * Mathf.Deg2Rad * 200;
                    //transform.localRotation = Quaternion.Slerp(transform.localRotation, m_TransformTargetRot, 5 * Time.deltaTime);
                    _rubicCube.transform.Rotate(Vector3.up, x);
                    _rubicCube.transform.Rotate(Vector3.right, y);
                    _rubicCube.transform.Rotate(Vector3.back, z);
                    //_rubicCube.transform.rotation = Quaternion.Euler(x, y, 0);
                }
            }
        }        
    }
}
